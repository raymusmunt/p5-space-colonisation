function Branch(parent, pos, dir){
	this.pos = pos;
	this.parent = parent;
	this.dir = dir;
	this.origDir = this.dir.copy();
	this.count = 0;
	this.age = 1;

	this.reset = function () {
		this.dir = this.origDir.copy();
		this.count = 0;
	}

	this.next = function () {
		var nextPos = p5.Vector.add(this.pos, this.dir);
		var nextBranch = new Branch(this, nextPos, this.dir.copy());
		return nextBranch;
	};
	this.show = function(_thickness) {
		if (parent !== null) {
			strokeWeight(_thickness);
			stroke(200);

			line(this.pos.x, this.pos.y, this.parent.pos.x, this.parent.pos.y);
		}
	};
}