var tree;
var max_dist = 50;
var min_dist = 7;
var showLeaves = false;

function setup(){
	createCanvas(800, 600);
	tree = new Tree();
}

function onMousePressed () {
	showLeaves = !showLeaves;
}

function draw(){
	background(51);

	tree.show();
	tree.grow();
}
